# Python-Terraform Docker Image

This project builds a Docker image based on the official GitLab CI image, with the latest versions of Python and Terraform installed.

## Usage

The image can be used in GitLab CI/CD pipelines or for local development purposes.

## Building the Image

The image is automatically built and pushed to the GitLab Docker registry through the GitLab CI/CD pipeline defined in `.gitlab-ci.yml`.
