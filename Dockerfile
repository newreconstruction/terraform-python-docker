ARG PYTHON_VERSION

# Use the official Python image with the specified version
FROM python:${PYTHON_VERSION}


ARG TERRAFORM_VERSION

# Install Terraform with the specified version
RUN apt-get update && apt-get install -y wget unzip \
    && wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin/ \
    && rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && apt-get clean

# Set the working directory in the container
WORKDIR /usr/src/app

# Verify installations
RUN python --version
RUN terraform version
